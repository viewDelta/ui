//Retriving Cookie Name
function getCookie(name) {
    // Split cookie string and get all individual name=value pairs in an array
    var cookieArr = document.cookie.split(";");

    // Loop through the array elements
    for (var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");

        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if (name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }
    // Return null if not found
    return null;
}

//deleteAllCookies will remove all cookies from the application
function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

//Do logout
function logout() {
    deleteAllCookies();
    location.reload();
}

//Validate Email
function validateEmail(email) {
    var re = new RegExp("^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$");
    return re.test(email);
}

//Validate Password
function validatePassword(pwd) {
    return (pwd.length >= 6);
}

//checkCookie if session cookie is present redirect to home
function checkCookie(){
    if(getCookie("session")!==null){
        window.location.replace(location.protocol + "//" + location.hostname + "/home")
      //   window.location.replace(location.protocol + "//" + location.hostname +":8989"+ "/home")
    }
}