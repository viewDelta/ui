#Simple Docker Image
FROM ubuntu:14.04
LABEL Author="Abinash <apattajoshi@outlook.com>" 
EXPOSE 8989 

#Work Location Image
ARG APP_HOME="/usr/local/UI/" 
ARG LOG_DIR="/var/retailGo/logs/"
ARG APP="bin/" 

RUN mkdir -p ${APP_HOME} 
RUN mkdir -p ${LOG_DIR} 
RUN chmod 777 -R ${LOG_DIR}
#Log File Directory Exposed which will be mapped during containerization
VOLUME [ ${LOG_DIR} ]

COPY ${APP} ${APP_HOME} 
WORKDIR ${APP_HOME}
CMD ["./ui"]