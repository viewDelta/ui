module bitbucket.org/viewDelta/ui

go 1.14

require (
	bitbucket.org/viewDelta/service-core v0.0.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
)

replace bitbucket.org/viewDelta/service-core => ../service-core
