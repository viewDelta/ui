.PHONY: clean
clean:
	rm -rf bin

.PHONY: build
build: clean
	#Ubuntu
	GOARCH=amd64 GOOS=linux go build -o bin/ui
	chmod +x bin/ui
	cp *.yml bin/
	cp -r pages bin/
