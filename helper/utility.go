package helper

import (
	"net/http"
	"os"

	"bitbucket.org/viewDelta/service-core/repository/roles"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/gommon/log"
)

//UserDetails from session
type UserDetails struct {
	UID                                  string
	Email, Password, FirstName, LastName string
	Role                                 []string
}

//GetPageDir will return path towards relative to binary directory
func GetPageDir() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return ""
	}
	return dir + "/pages/" //pages directory where pages will be kept
}

//GetSessionCookieContents - Get All Information from Cookie
func GetSessionCookieContents(cookie *http.Cookie) (*UserDetails, error) {
	roleRepo := roles.New()
	userRepo := users.New()
	//Fetching role inside roles
	clamis, err := utils.GetTokenDetails(cookie.Value)
	if err != nil {
		log.Errorf("Token Decoding Error - [%v]", err)
		return nil, err
	}
	var roles []string
	//Fetching all roles from database
	allRoles, err := roleRepo.GetAll()
	if err != nil {
		log.Errorf("All Roles Fetch Error - [%v]", err)
		return nil, err
	}
	for _, v := range clamis.Role {
		for _, ref := range allRoles {
			if ref.RoleID == v {
				roles = append(roles, ref.RoleName)
			}
		}
	}
	user, err := userRepo.FindUser(clamis.Email)
	if err != nil {
		log.Errorf("Error While fetching uid - [%v]", err)
	}
	tokenDetails := &UserDetails{
		Email:     clamis.Email,
		FirstName: clamis.FirstName,
		LastName:  clamis.LastName,
		Role:      roles,
		UID:       user.UID,
	}
	return tokenDetails, nil
}
