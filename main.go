package main

import (
	"context"
	"html/template"
	"io"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/viewDelta/service-core/config"
	endpoints "bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/ui/handlers"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
)

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	//Read Property Loader
	prop := config.GetConfiguration()
	//Echo Router
	router := echo.New()
	router.Logger.SetLevel(log.INFO)

	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob("*pages/*.html")),
	}
	router.Renderer = renderer

	//Handling Endpoints
	router.Static("/", "pages/assets")
	router.File(endpoints.IndexPage, "pages/login.html")
	router.File(endpoints.ErrorPage, "pages/error.html")
	router.File(endpoints.ResetEmailEnterPage, "pages/resetPwd.html")
	router.GET(endpoints.HomePage, handlers.HomePAGE)
	router.GET(endpoints.ProfilePage, handlers.Profile)
	router.POST(endpoints.ResetPage, handlers.ResetPage)

	log.Infof("%v-%v Started on --> %v", prop.Application.Name, prop.Application.Version, prop.Application.Port)
	// Start server
	go func() {
		if err := router.Start(":" + prop.Application.Port); err != nil {
			log.Infof("shutting down the server %v", err)
		}
	}()
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//Closing the log file upon receiving the signal
	config.CloseLogFile()
	if err := router.Shutdown(ctx); err != nil {
		router.Logger.Fatal(err)
	}
}
