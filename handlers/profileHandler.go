package handlers

import (
	"net/http"

	endpoints "bitbucket.org/viewDelta/service-core/constants"
	settings "bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/ui/helper"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
)

//HomePAGE will handle index or home page
func HomePAGE(c echo.Context) error {
	defer func() error {
		if r := recover(); r != nil {
			log.Warnf("Application PANIC error --> ", r)
			return c.Redirect(http.StatusSeeOther, endpoints.ErrorPage)
		}
		return nil
	}()
	cookie, err := c.Cookie(settings.COOKIE_TOKEN_NAME)
	if err != nil {
		log.Warnf("Cookie Not Found [%v] [Error - %v]", settings.COOKIE_TOKEN_NAME, err)
		return c.Redirect(http.StatusSeeOther, endpoints.IndexPage)
	}
	userDetails, err := helper.GetSessionCookieContents(cookie)
	value := struct {
		FName string
	}{
		FName: userDetails.FirstName,
	}
	return c.Render(http.StatusOK, "Thome.html", value)
}

//Profile will show the user profile
func Profile(c echo.Context) error {
	defer func() error {
		if r := recover(); r != nil {
			log.Warnf("Application PANIC error --> ", r)
			return c.Redirect(http.StatusSeeOther, endpoints.ErrorPage)
		}
		return nil
	}()
	cookie, err := c.Cookie(settings.COOKIE_TOKEN_NAME)
	if err != nil {
		log.Warnf("Cookie Not Found [%v] [Error - %v]", settings.COOKIE_TOKEN_NAME, err)
		return c.Redirect(http.StatusSeeOther, endpoints.IndexPage)
	}
	userDetails, err := helper.GetSessionCookieContents(cookie)
	value := struct {
		FName, LName, Roles, UID, Email string
	}{
		FName: userDetails.FirstName,
		LName: userDetails.LastName,
		UID:   userDetails.UID,
		Email: userDetails.Email,
	}
	value.Roles = value.Roles + " | "
	for _, roleName := range userDetails.Role {
		value.Roles = value.Roles + roleName + " | "
	}
	return c.Render(http.StatusOK, "TProfile.html", value)
}

//ResetPage will display password reset page, if token is present
func ResetPage(c echo.Context) error {
	defer func() error {
		if r := recover(); r != nil {
			log.Warnf("Application PANIC error --> ", r)
			return c.Redirect(http.StatusSeeOther, endpoints.ErrorPage)
		}
		return nil
	}()
	token := c.FormValue("resetToken")
	if token == "" {
		log.Warn("Reset Token Not Found")
		return c.Redirect(http.StatusSeeOther, endpoints.IndexPage)
	}
	data := struct {
		TOKEN string
	}{
		TOKEN: token,
	}
	return c.Render(http.StatusOK, "TsetPwd.html", data)
}
